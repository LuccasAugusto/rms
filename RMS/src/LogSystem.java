import java.util.Scanner;

public class LogSystem {
	
	private Scanner scan = new Scanner(System.in);
	
	public boolean login() {

		String username;
		String password;

		System.out.println("User name: ");
		username = this.scan.nextLine();
		System.out.println("\nPassword: ");
		password = this.scan.nextLine();

		return DataBase.check(username, password);
	}

}
