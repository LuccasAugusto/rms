import java.util.Hashtable;

public class DataBase {

	private static Hashtable<String, String> userPass = new Hashtable<String, String>();
	private static Hashtable<String, User> userData = new Hashtable<String, User>();
	private static Hashtable<Integer, Resource> resourceData = new Hashtable<Integer, Resource>();

	// verifica nome de usu�rio e senha.
	protected static boolean check(String userName, String password) {
		return userPass.get(userName).equals(password);
	}

	public static User getUserData(String userName) {
		return userData.get(userName);
	}

	public static Resource getResourceData(int id) {
		return resourceData.get(id);
	}

	public static boolean userExist(String userName) {
		return userData.containsKey(userName);
	}

	public static void setUserData(String userName, User user) {
		userData.put(userName, user);
	}

	public static boolean newUserNamePass(String newUserName, String NewPass, User user) {
		boolean verify = userExist(newUserName);

		if (verify) {
			return true;
		} else {
			userPass.put(newUserName, NewPass);
			userData.put(newUserName, user);
			return false;
		}
	}

	public static boolean newResource(int id, Resource resource) {
		boolean exist = resourceExist(id);

		if (exist) {
			System.out.println("This resource already exist.");
			return false;
		}

		resourceData.put(id, resource);
		return true;
	}

	public static boolean resourceExist(int id) {
		return resourceData.containsKey(id);
	}

	public static Hashtable<Integer, Resource> getHashResourceData() {
		return resourceData;
	}

}
