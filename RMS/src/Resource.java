 import java.util.Hashtable;

public class Resource {
	
	private static int index = 0;
	private static int numResource = 0;
	private final int id;
	private String startData = new String();
	private String endData =  new String();
	private String status = new String();
	private boolean allocated;
	private Hashtable<String,String> historic = new Hashtable<String,String>();
	
	Resource(){
		this.status="FREE";
		this.allocated = false;
		this.id = generateId();
		this.addNumResource();
	}
	
	Resource(String status){
		this.status = status;
		this.id = generateId();
		this.allocated = false;
		this.addNumResource();
	}
	
	private int generateId() {
		return ++index;
	}
	
	public String getStartData() {
		return startData;
	}
	
	public void setStartData(String startData) {
		this.startData = startData;
	}
	
	public String getEndData() {
		return endData;
	}
	public void setEndData(String endData) {
		this.endData = endData;
	}
	public String getStatus() {
		return status;
	}
	public String setStatus(String status) {
		this.status = status;
		return this.status;
	}	
	public String getHistoric(int id) {
		return historic.get(id);
	}

	public void putHistoric(String user, String activity) {
		this.historic.put(user, activity);
	}

	public int getId() {
		return id;
	}

	public boolean isAllocated() {
		return allocated;
	}

	public void setAllocated(boolean allocated) {
		this.allocated = allocated;
	}

	public static int getNumResource() {
		return numResource;
	}

	public static void addNumResource() {
		++Resource.numResource;
	}	
		
}
