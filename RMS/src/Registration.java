import java.util.Scanner;

public class Registration {

	private static Scanner scan = new Scanner(System.in);

	public static boolean newUser() {
		System.out.println("Name: ");
		String name = scan.nextLine();
		System.out.println("\nEmail: ");
		String email = scan.nextLine();
		while (true) {
			String status = getStatusUser();
			if (status == "WRONG") {
				System.out.println("INCORRECT OPTION. TRY AGAIN!");
			} else if (status == "OUT") {
				System.out.println("GOING OUT!");
				break;
			} else {
				User newUser = new User(name, email, status);
				userPass(newUser);
				System.out.println("New user registered!");
				return true;
			}
		}
		return false;
	}
	
	private static void userPass(User newUser){
		boolean verify = true;
		
		while(verify){
			System.out.println("\nLogin Name:");
			String logName = scan.nextLine();
			scan.nextLine();
			
			verify = DataBase.userExist(logName);
			
			if(verify){
				System.out.println("This logname already exists! Tray again!");	
			}else if(!verify){
				System.out.println("\nPassword: ");
				String pass = scan.nextLine();
				System.out.println("\n\n\n\n\n");
				DataBase.newUserNamePass(logName, pass, newUser);
			}
		}	
		
	}
	
	private static String getStatusUser() {
		System.out.println("What's the status?\n" 
						  + "1 - TEACHER;\n" 
						  + "2 - RESEARCHER;\n" 
						  + "3 - STUDANT;\n" 
						  + "4 - ADM;\n"
						  + "5 - OUT and CANCEL.\n");
		int status = scan.nextInt();
		if (status == 1) {
			return "TEACHER";
		} else if (status == 2) {
			return "RESEARCHER";
		} else if (status == 3) {
			return "STUDANT";
		} else if (status == 4) {
			return "ADM";
		} else if (status == 5) {
			return "OUT";
		} else {
			return "WRONG";
		}
	}

	public static boolean newResource() {		
		while (true) {
			String status = getStatusResource();
			if (status == "WRONG") {
				System.out.println("INCORRECT OPTION. TRY AGAIN!");
			} else if (status == "OUT") {
				System.out.println("GOING OUT!");
				break;
			} else {
				Resource newResource = new Resource(status);
				System.out.println("New resource registered!");
				return true;
			}
		}
		return false;
	}
	
	private static String getStatusResource() {
		System.out.println("What's the status?" 
						  + "1 - CLASSROOM;" 
						  + "2 - LAB;" 
						  + "3 - AUDITORIUM;" 
						  + "4 - PROJECTOR;"
						  + "5 - OUT and CANCEL.");
		int status = scan.nextInt();
		if (status == 1) {
			return "CLASSROOM";
		} else if (status == 2) {
			return "LAB";
		} else if (status == 3) {
			return "AUDITORIUM";
		} else if (status == 4) {
			return "PROJECTOR";
		} else if (status == 5) {
			return "OUT";
		} else {
			return "WRONG";
		}
	}
	
	
}
