import java.util.Scanner;

public class Command {
	private Scanner scan = new Scanner(System.in);

	public void menuStart() {
		boolean logout = true;
		while(logout){
			System.out.println("1 - User Registration;\n"
						  	  +"2 - Resource Registration;\n"
				              +"3 - User Data;\n"
				              +"4 - Resource Data;\n"
				              +"5 - Activities Report;\n"
				              +"6 - Allocate Resource;\n"
				              +"7 - Logout.\n");
			int command = this.scan.nextInt();
			logout = startCommand(command);
		}
	}

	private boolean startCommand(int command) {
		if (command == 1) {
			return Registration.newUser();
		} else if (command == 2) {
			return Registration.newResource();
		} else if (command == 3) {
			System.out.println("\n\n\n" + userData().toString());
			return true;
		} else if (command == 4) {
			System.out.println("\n\n\n" + resourceData().toString());
			return true;
		} else if (command == 5) {
			activitiesReport();
			return true;
		} else if (command == 6) {
			allocateResource();
			return true;
		} else if (command == 7) {
			return logout();
		} else {
			System.out.println("This option don't exist.");
			return true;
		}
	}

	private boolean logout() {
		return false;
	}

	private void allocateResource() {

		System.out.println("Give the id:");
		int id = scan.nextInt();
		Resource item = DataBase.getResourceData(id);
		Allocation alloc = new Allocation(item);

		boolean isAlloc = item.isAllocated();

		if (isAlloc) {
			System.out.println("Resource allocated.");
		} else {
			System.out.println("Imcomplet process.");
		}
	}

	private void activitiesReport() {
		System.out.println(DataBase.getHashResourceData().toString());
	}

	private Resource resourceData() {
		System.out.println("Set id: ");
		int id = scan.nextInt();
		boolean exist = DataBase.resourceExist(id);

		if (exist) {
			return DataBase.getResourceData(id);
		}
		System.out.println("This resource doesn't exist!");
		return null;
	}

	private User userData() {
		System.out.println("Set login name: ");
		String logName = scan.nextLine();
		boolean exist = DataBase.userExist(logName);

		if (exist) {
			return DataBase.getUserData(logName);
		}
		System.out.println("This user doesn't exist!");
		return null;
	}

	private void newResource() {
		// TODO Auto-generated method stub

	}

}
