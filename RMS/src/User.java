import java.util.Hashtable;

public class User {

	private static int numUser;
	private String name = new String();
	private String email = new String();
	private String status = new String();
	private Hashtable<Integer,String> historic = new Hashtable<Integer,String>();
	
	User(){
		++numUser;
	}
	
	User(String name, String email, String status){
		this.name = name;
		this.email = email;
		this.status = status;
	}
	
	public void showProfile(){
		System.out.println(this.toString());
		System.out.println("Name: "+this.name
						  +"\nEmail: "+this.email
						  +"\nStatus: "+this.status
						  +"\n"+this.historic.toString());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getHistoric(String name) {
		return historic.get(name);
	}

	public void putHistoric(int id, String historic) {
		this.historic.put(id, historic);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static int getNumUser() {
		return numUser;
	}

}
