import java.util.Scanner;

public class Allocation {
	
	private final String FREE  = "FREE";
	private final String IALLP = "IN ALLOCATION PROCESS";
	private final String ALLOC = "ALLOCATED";
	private final String INPRO = "IN PROGRESS";
	private final String CONCL = "CONCLUDED"; 
	private String answer;
	private Scanner scan = new Scanner(System.in);;
	
	Allocation(){
		this.answer = new String();
	}
	
	Allocation(Resource item){
		this.allocate(item);
		this.answer = new String();
	}
	
	public void allocate(Resource item){	
		if(item.getStatus() == this.FREE){
			changeStatus(item);
		}else{
			System.out.println("ID: "+item.getId()+" STATUS: "+item.getStatus() 
					+"\nThis resource cannot be allocated");
		}
	}

	private void changeStatus(Resource item){
		if(item.getStatus() == this.FREE){
			freeStatus(item);	
		}else if(item.getStatus() == this.IALLP){
			iallpStatus(item);	
		}else if(item.getStatus() == this.ALLOC){
			allocatedStatus(item);	
		}else if(item.getStatus() == this.INPRO){
			inproStatus(item);
		}
	}
	
	private void freeStatus(Resource item){
		System.out.println("ID: "+item.getId()+" STATUS: " + item.getStatus() 
				+"\n\nDo you want allocate this resource?"+"\ny = YES" +" n = NO");
		
		this.answer = this.scan.nextLine();
		freeQuiz(item);		
	}
	
	private void freeQuiz(Resource item){
		if(this.answer.equalsIgnoreCase("y")){
			System.out.println("ID: "+item.getId()+" STATUS: "+item.setStatus(this.IALLP));
		}else if(this.answer.equalsIgnoreCase("n")){
			System.out.println("Ok! Get out of the allocation system.");
		}else{
			System.out.println("Unexpected response.");
		}
		this.answer = null;		
	}
	
	private void iallpStatus(Resource item){
		System.out.println("STATUS: " + item.getStatus() 
				+"\n\nAll the basic information has been checked?"+"\ny = YES" + " n = NO");
				
		iallpQuiz(item);
	}
	
	private void iallpQuiz(Resource item){
		this.answer = this.scan.nextLine();
		
		if(this.answer.equalsIgnoreCase("y")){
			System.out.println("ID: "+item.getId()+" STATUS: "+item.setStatus(this.ALLOC));
		}else if(this.answer.equalsIgnoreCase("n")){
			questionProcess(item);
		}else{
			System.out.println("Unexpected response.");
		}
		this.answer = null;
	}

	private void allocatedStatus(Resource item) {
		System.out.println("STATUS: " + item.getStatus() 
		+"\n\nThe allocation is confirmed?"+"\ny = YES" + " n = NO");
		
		allocatedQuiz(item);		
	}

	private void allocatedQuiz(Resource item) {
		this.answer = this.scan.nextLine();
		
		if(this.answer.equalsIgnoreCase("y")){
			System.out.println("ID: "+item.getId()+" STATUS: "+item.setStatus(this.INPRO));
		}else if(this.answer.equalsIgnoreCase("n")){
			questionProcess(item);
		}else{
			System.out.println("Unexpected response.");
		}
		this.answer = null;		
	}

	private void inproStatus(Resource item) {
		System.out.println("ID: "+item.getId()+"STATUS: " + item.getStatus() 
		+"\n\nAll requirements were accomplished to conclude the process?"+
		"\ny = YES" +" n = NO");

		inproQuiz(item);
	}
	
	private void inproQuiz(Resource item) {
		this.answer = this.scan.nextLine();
		
		if(this.answer.equalsIgnoreCase("y")){
			System.out.println("ID: "+item.getId()+"STATUS: "+
					item.setStatus(this.FREE)+"\n"+this.CONCL);
		}else if(this.answer.equalsIgnoreCase("n")){
			System.out.println("Waiting for accomplishment of requirements.");
		}else{
			System.out.println("Unexpected response.");
		}
		this.answer = null;
	}

	private void questionProcess(Resource item){
		System.out.println("Do you want continue this process?"
				+ "\ny = YES" + " n = NO");
		
		this.answer = this.scan.nextLine();
		if(this.answer.equalsIgnoreCase("y")){
			System.out.println("ID: "+item.getId()+"STATUS: " + item.getStatus());
		}else if(this.answer.equalsIgnoreCase("n")){
			System.out.println("ID: "+item.getId()+"STATUS: " + item.setStatus(this.FREE));
		}else{
			System.out.println("Unexpected response!");
		}
		this.answer = null;
	}	
}
